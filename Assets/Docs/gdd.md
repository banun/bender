[TOC]

---

### Game Design

#### Concept
Create a game in which user has super powers, modeled after Avatar: the Last Airbender
User is able to control elements like fire, earth, water, air.

#### Objective
1. Learn the crap out of your powers (Day)
2. Hit all targets - waves of enemies (Night)
3. Survive as long as possible (Days & Nights)

#### Gameplay Mechanics
Conjure elements, manipulate, and shoot them

Limitations:
Can only conjure/manipulate elements that already exist

Use elements to block and stuff

#### Level Design
Centered around single point
Campire - dimly lit, vision limited?
Waves of enemies are coming at you from all around

[Back to top ^](#)

---

### Technical

#### Scenes
* [list the different scenes/screens used in the game]

#### Controls/Input
WIP

#### Components/Non-Component Scripts
* Components
  * [list Components needed and some basic information about required implementation]
* Non-Component Scripts
  * [list Non-Component Script files you will need to create]

[Back to top ^](#)

---

### MVP Milestones
[x] Conjure things from the ground
[x] Move it in some way
[x] Maybe shoot it
[] Make it feel damn good

#### Day 1, Part 1
* Game Design Document

#### Day 1, Part 2
* Some test MVPs for conjuring

#### Day 2 Overall Goals
* Cement bending mechanics - limitation & controls
* Getting it to feel right

#### Day 2, Part 1
* Implement limitations for manipulations
* Make it feel harder
  * Rumble + Shudder
  * Force/Momentum-based movement - anything but 1-to-1

#### Day 2, Part 2
* Still making things feel good
Minor branch: Add basic enemies and attack mechanics

#### Day 3, Part 1
* Fix up environment/scene
* Enemy and attack mechanics
* Change control scheme:
  - Grip with two hands and pull: wall
  - Grip with one hand: spike
  - Trigger: summon rock
  - Trigger release: shoot rock in direction
* Reticle freeze and change color on grip/trigger
* Shudder + Sound
* Onboarding

#### Day 3, Part 2
* [goals for the 2nd half of the day, should be finishing the polish]

---

### Stretch Goals
- Extra game logic:
  - Getting hurt
  - Different enemies
  - Enemy flocking
  [x] Restart scene
  [x] Day and Night
- Make it look pretty

[Back to top ^](#)
﻿using UnityEngine;
using System.Collections;

public class OtherHandController : MonoBehaviour {

  private SteamVR_TrackedObject trackedObj;
  private SteamVR_Controller.Device device;
  private int deviceIdx;

  public GameObject otherHand;
  private BendController otherHandCtrl;

  void Awake () {
    trackedObj = GetComponent<SteamVR_TrackedObject>();
  }
	// Use this for initialization
	void Start () {
    if ((int)trackedObj.index != deviceIdx)
    {
        deviceIdx = (int)trackedObj.index;
        device = SteamVR_Controller.Input(deviceIdx);
    }

    otherHandCtrl = otherHand.GetComponent<BendController>();
	}

	// Update is called once per frame
	void Update () {
     if ((int)trackedObj.index != deviceIdx)
     {
         deviceIdx = (int)trackedObj.index;
         device = SteamVR_Controller.Input(deviceIdx);
     }

    if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
    {
      otherHandCtrl.ChangeOtherHandState(true);
    }

    if (device.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
    {
      otherHandCtrl.ChangeOtherHandState(false);
    }
	}
}

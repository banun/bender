﻿using UnityEngine;
using System.Collections;

public class DayAndNight : MonoBehaviour {

    public float sunRotationSpeed = 5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(new Vector3(sunRotationSpeed * Time.deltaTime, 0f, 0f));
    }
}

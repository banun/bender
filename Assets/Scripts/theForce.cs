﻿using UnityEngine;
using System.Collections;

public class theForce : MonoBehaviour {

    public SteamVR_TrackedObject trackedObject;
    public SteamVR_Controller.Device device;
    public float forcePower = 1000f;

    // Use this for initialization
    void Start () {

        trackedObject = GetComponent<SteamVR_TrackedObject>();
	
	}
	
	// Update is called once per frame
	void Update () {

        device = SteamVR_Controller.Input((int)trackedObject.index);
	
	}

    void OnTriggerStay(Collider other)
    {
        if (other.attachedRigidbody)
        {
            other.attachedRigidbody.AddForce(forcePower * device.velocity);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class summonRock : MonoBehaviour {

    public GameObject miniSpurtPrefab;
    public GameObject spurtPrefab;
    private GameObject rockPrefab;

    public float rockHeight;
    public float raiseHeight = 1f;
    public float floatRate = .05f;
    private Rigidbody rockRb;
    private GameObject rock;

    private bool spurting = true;
    public bool creating = true;

    public float forcePower = 3000f;

    // Use this for initialization
    void Start () {
        GameObject miniSpurtObj = Instantiate(miniSpurtPrefab) as GameObject;
        miniSpurtObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

        rock = transform.GetChild(0).gameObject;
        rockHeight = rock.GetComponent<Renderer>().bounds.size.y;
        //Debug.Log(rockHeight);

        //rock.transform.position = new Vector3(transform.position.x, -rockHeight*.8f, transform.position.z);
        rock.transform.rotation = transform.rotation;

        rockRb = rock.GetComponent<Rigidbody>();
        rockRb.isKinematic = true;

    }



    // Update is called once per frame
    void Update () {
        if(rock.transform.position.y < raiseHeight && creating)
        {
            rock.transform.position += new Vector3(0, floatRate, 0);
            if (rock.transform.position.y >= raiseHeight)
            {
                creating = false;
                rockRb.isKinematic = false;
                rockRb.useGravity = false;
            }
        }

    }
    

    public void Shoot(Vector3 controllerForward)
    {
        rockRb.AddForce(forcePower * controllerForward);
        rockRb.useGravity = true;
    }
}

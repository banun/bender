﻿using UnityEngine;
using System.Collections;

public class BendController : MonoBehaviour
{

    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device device;
    private int deviceIdx;

    private SteamVR_TrackedObject otherTrackedObj;
    private SteamVR_Controller.Device otherDevice;
    private int otherDeviceIdx;

    #region placeholder_variables

    public float maxDistance = Mathf.Infinity;
    public float minMoveRate;
    public ushort minPulseTime = 300;
    public float pulseRate;
    public float moveThreshold;
    public Vector3 smallReticle = new Vector3(0.2f, 0.2f, 0.2f);
    public Vector3 largeReticle = new Vector3(1f, 1f, 1f);

    #endregion

    #region creation_variables

    public Transform reticle;
    private GameObject target;

    public GameObject wallPrefab;
    public GameObject spikePrefab;
    public GameObject projectilePrefab;
    public GameObject otherHand;
    private Renderer wallRenderer;

    private Vector3 prevHandPosition = Vector3.zero;
    // private Vector3 createdAtPosition = Vector3.zero;

    private bool creatingThing;
    private Texture thingTexture;

    private GameObject currentRock;
    private GameObject spike;

    private float moveRate;
    private ushort pulseTime;
    private bool otherFrame;
    private bool isAiming = true;

    private bool otherHandIsGripped;

    #endregion

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        otherTrackedObj = otherHand.GetComponent<SteamVR_TrackedObject>();
        pulseTime = minPulseTime;
        moveRate = minMoveRate;
    }

    void Start()
    {
        if ((int)trackedObj.index != deviceIdx)
        {
            deviceIdx = (int)trackedObj.index;
            device = SteamVR_Controller.Input(deviceIdx);
        }

        if ((int)otherTrackedObj.index != otherDeviceIdx)
        {
            otherDeviceIdx = (int)otherTrackedObj.index;
            otherDevice = SteamVR_Controller.Input(otherDeviceIdx);
        }
    }

    void Update()
    {
        if ((int)trackedObj.index != deviceIdx)
        {
            deviceIdx = (int)trackedObj.index;
            device = SteamVR_Controller.Input(deviceIdx);
        }

        if ((int)otherTrackedObj.index != otherDeviceIdx)
        {
            otherDeviceIdx = (int)otherTrackedObj.index;
            otherDevice = SteamVR_Controller.Input(otherDeviceIdx);
        }

        #region aim-reticle

        if (isAiming)
        {

            // Choose target area
            RaycastHit hit;
            bool foundHit;

            foundHit = Physics.Raycast(transform.position, transform.forward, out hit, maxDistance);

            // TODO: Add checking for valid spots

            if (foundHit && hit.transform.gameObject.CompareTag("Terrain"))
            {
                // Debug.Log("Found hit");

                // position reticle
                reticle.position = hit.point;
                // reticle.rotation = Quaternion.LookRotation(hit.normal);
                reticle.rotation = Quaternion.LookRotation(hit.normal) * Quaternion.Euler(90, 0, 0);
                reticle.gameObject.SetActive(true);

                // retrieve texture from spot
                thingTexture = hit.collider.gameObject.GetComponent<Renderer>().material.GetTexture("_MainTex");
            }
            else
            {
                reticle.gameObject.SetActive(false);
            }
        }

        #endregion

        #region spike-creation
        if (reticle.gameObject.activeSelf && !otherHandIsGripped && device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {

          spike = Instantiate(spikePrefab) as GameObject;
          wallRenderer = spike.transform.GetChild(0).gameObject.GetComponent<Renderer>();

          spike.transform.position = reticle.position;
          spike.transform.rotation = reticle.rotation;

          device.TriggerHapticPulse(2000);

          wallRenderer.material.SetTexture("_MainTex", thingTexture);
        }

        // if (reticle.gameObject.activeSelf && !otherHandIsGripped && device.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        // {
        //   spike.GetComponent<summonRock>().Shoot(transform.forward);
        // }

        #endregion

        #region wall-creation

        if (reticle.gameObject.activeSelf && otherHandIsGripped)
        {
            reticle.GetChild(0).gameObject.SetActive(true);
        }

        // create wall if other hand is gripped
        if (reticle.gameObject.activeSelf && otherHandIsGripped && device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            isAiming = false;

            // activate children
            foreach (Transform child in reticle)
            {
                child.gameObject.SetActive(true);
            }

            target = Instantiate(wallPrefab) as GameObject;

            // get child (model)
            GameObject thing = target.transform.GetChild(0).gameObject;

            // position under floor
            wallRenderer = thing.GetComponent<Renderer>();
            target.transform.position = reticle.position;
            target.transform.rotation = reticle.rotation;

            // set texture
            wallRenderer.material.SetTexture("_MainTex", thingTexture);

            //deactivate reticle
            // reticle.gameObject.SetActive(false);
        }

        // wall manipulation & creation logic
        if (target && target.activeSelf && device.GetPress(SteamVR_Controller.ButtonMask.Grip))

        {
            // set start hand position and initiate creation
            if (prevHandPosition == Vector3.zero)
            {
                // Debug.Log("Creating Wall");
                prevHandPosition = transform.position;
                creatingThing = true;
            }

            if (creatingThing)
            {
                otherFrame = !otherFrame;
                // check previous position with current
                Vector3 movement = (transform.position - prevHandPosition) / Time.deltaTime;

                SummonRockwall wallSpurtControl = target.GetComponent<SummonRockwall>();
                wallSpurtControl.setMove(false);
                // TODO: simulate object shudder
                // if (!otherFrame) {
                //   target.transform.Translate(new Vector3(0, Random.value, 0) * Time.deltaTime * Random.Range(0f, 5f));
                // } else {
                //   target.transform.position = createdAtPosition;
                // }

                // haptic pulse increase over time
                float pulse = Mathf.Clamp((float)pulseTime * (1 + (pulseRate * Time.deltaTime)), 100f, 3500f);
                pulseTime = (ushort)pulse;
                device.TriggerHapticPulse(pulseTime);
                otherDevice.TriggerHapticPulse(pulseTime);

                if (pulse > 1500f)
                {
                    moveRate = minMoveRate * 2;
                }
                // limited upwards Y movement
                if (movement.y > moveThreshold)
                {
                    // Debug.Log("You moved your hand up");


                    wallSpurtControl.setMove(true);
                    target.transform.position = new Vector3(target.transform.position.x, Mathf.Clamp(target.transform.position.y + moveRate, 0f, wallRenderer.bounds.size.y), target.transform.position.z);

                    // FOR ROTATABLE WALLS: target.transform.position += target.transform.up * moveRate;
                }

                // downwards Y movement
                // TODO: fix moverate if going in different direction
                if (movement.y < -moveThreshold)
                {
                    wallSpurtControl.setMove(true);
                    target.transform.position = new Vector3(target.transform.position.x, Mathf.Clamp(target.transform.position.y - moveRate, 0f, wallRenderer.bounds.size.y), target.transform.position.z);
                }

            }

            prevHandPosition = transform.position;
        }

        // if not currently being created
        // reset
        if (creatingThing && device.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            // Debug.Log("No longer creating");
            creatingThing = false;
            prevHandPosition = Vector3.zero;
            pulseTime = minPulseTime;
            moveRate = minMoveRate;
            isAiming = true;

            foreach (Transform child in reticle)
            {
                child.gameObject.SetActive(false);
            }
        }

        #endregion

        #region projectile-creation
        // replace with target
        if (reticle.gameObject.activeSelf && device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            // instantiate and position rock
            currentRock = Instantiate(projectilePrefab) as GameObject;

            currentRock.transform.position = reticle.position;
            // currentRock.transform.rotation = reticle.rotation;
            Renderer rockRenderer = currentRock.transform.GetChild(0).gameObject.GetComponent<Renderer>();
            currentRock.transform.position = reticle.position;
            // target.transform.rotation = reticle.rotation;

            // set texture
            rockRenderer.material.SetTexture("_MainTex", thingTexture);

            device.TriggerHapticPulse(2000);
            reticle.gameObject.SetActive(false);
        }

        if (currentRock && currentRock.activeSelf && device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            currentRock.GetComponent<summonRock>().Shoot(transform.forward);
            reticle.gameObject.SetActive(true);
        }


        #endregion

        // restart
        if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
        }



    }

    public void ChangeOtherHandState(bool gripped) {
      otherHandIsGripped = gripped;
    }


}

﻿using UnityEngine;
using System.Collections;

public class SummonRockwall : MonoBehaviour
{
    public bool moving = false;

    private bool hasBeenInstantiated;
    public GameObject spurtPrefab;

    // Use this for initialization
    void Start()
    {

    }

    public void setMove (bool isMoving)
    {
        moving = isMoving;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving && !hasBeenInstantiated)
        {
            GameObject spurtObj = Instantiate(spurtPrefab) as GameObject;
            spurtObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            hasBeenInstantiated = true;
        }


    }
}

﻿using UnityEngine;
using System.Collections;

public class MouseTester : MonoBehaviour {

    public GameObject rockPrefab;
    public GameObject spikePrefab;
    public GameObject wallPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            //GameObject rockObj = Instantiate(rockPrefab) as GameObject;
            //rockObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

            //GameObject spikeObj = Instantiate(spikePrefab) as GameObject;
            //spikeObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

            GameObject wallObj = Instantiate(wallPrefab) as GameObject;
            wallObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

        }

        if (Input.GetMouseButtonDown(1))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
        }
    }
}
